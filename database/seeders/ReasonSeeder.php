<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class ReasonSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('reasons')->insert([
            'title' => Str::random(10),
        ]);
        DB::table('reasons')->insert([
            'title' => Str::random(10),
        ]);
        DB::table('reasons')->insert([
            'title' => Str::random(10),
        ]);
        DB::table('reasons')->insert([
            'title' => Str::random(10),
        ]);
        DB::table('reasons')->insert([
            'title' => Str::random(10),
        ]);
    }
}
