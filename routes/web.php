<?php

use App\Http\Controllers\ClaimController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::match(['GET', 'POST'],'/', [ClaimController::class, 'index'])->name('claim.list');

Route::get('/claim/create', [ClaimController::class, 'create'])->name('claim.create');

Route::post('/claim/create', [ClaimController::class, 'store'])->name('claim.store');

Route::get('/claim/view/{id}', [ClaimController::class, 'view'])->name('claim.view');
