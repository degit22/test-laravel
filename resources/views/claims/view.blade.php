@extends('layouts.main')

@section('content')
    <div class="row g-3 my-4">
        <div class="col-12">
            <label for="name" class="form-label">ФИО</label>
            <input type="text" class="form-control" id="name" value="{{ $claim->name }}" disabled>
        </div>
        <div class="col-12">
            <label for="phone" class="form-label">Телефон</label>
            <input type="text" class="form-control" id="phone" value="{{ $claim->phone }}" disabled>
        </div>
        <div class="col-12">
            <label for="hospital" class="form-label">Поликлиника</label>
            <input type="text" class="form-control" id="hospital" value="{{ $claim->hospital }}" disabled>
        </div>
        <div class="col-12">
            <label for="reason_id" class="form-label">Повод</label>
            <input type="text" class="form-control" id="hospital" value="{{ $claim->reason->title }}" disabled>
        </div>
        <div class="col-12">
            <label for="content" class="form-label">Сообщение</label>
            <textarea class="form-control" id="content" disabled>{{ $claim->content }}</textarea>
        </div>
    </div>
@endsection
