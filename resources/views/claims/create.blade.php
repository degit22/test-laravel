@extends('layouts.main')

@section('content')
    <form class="row g-3 my-4" action="{{ route('claim.store') }}" method="POST">
        @csrf
        <div class="col-12">
            <label for="name" class="form-label">ФИО</label>
            <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" name="name" value="{{ old('name') }}">
            @error('name')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="col-12">
            <label for="phone" class="form-label">Телефон</label>
            <input type="text" class="form-control @error('phone') is-invalid @enderror" id="phone" name="phone" value="{{ old('phone') }}">
            @error('phone')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="col-12">
            <label for="hospital" class="form-label">Поликлиника</label>
            <input type="text" class="form-control @error('hospital') is-invalid @enderror" id="hospital" name="hospital" value="{{ old('hospital') }}">
            @error('hospital')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="col-12">
            <label for="reason_id" class="form-label">Повод</label>
            <select id="reason_id" class="form-select @error('reason_id') is-invalid @enderror" name="reason_id">
                <option>Выберите...</option>
                @foreach($reasons as $reason)
                    <option value="{{ $reason->id }}" @if(old('reason_id') == $reason->id) selected @endif>{{ $reason->title }}</option>
                @endforeach
            </select>
            @error('reason_id')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="col-12">
            <label for="content" class="form-label">Сообщение</label>
            <textarea class="form-control @error('content') is-invalid @enderror" id="content" name="content">{{ old('content') }}</textarea>
            @error('content')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="col-12">
            <button type="submit" class="btn btn-primary">Сохранить</button>
        </div>
    </form>
@endsection
