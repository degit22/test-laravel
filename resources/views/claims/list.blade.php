@extends('layouts.main')

@section('content')
    <h1>Список обращений</h1>
    <form class="d-flex my-4" action="{{ route('claim.list') }}" method="POST">
        @csrf
        <input class="form-control me-2" type="search" name="search" placeholder="ФИО, Телефон, Поликлиника" aria-label="Поиск">
        <button class="btn btn-primary" type="submit">Поиск</button>
    </form>
    <div class="table-responsive">
        <table class="table table-striped table-sm">
            <thead>
            <tr>
                <th scope="col">ФИО</th>
                <th scope="col">Телефон</th>
                <th scope="col">Поликлиника</th>
                <th scope="col">Повод</th>
            </tr>
            </thead>
            <tbody>
            @forelse($claims as $claim)
                <tr>
                    <td><a href="{{ route('claim.view', ['id' => $claim->id]) }}"><i class="fa-solid fa-comment"></i> {{ $claim->name }}</a></td>
                    <td>{{ $claim->phone }}</td>
                    <td>{{ $claim->hospital }}</td>
                    <td>{{ $claim->reason->title }}</td>
                </tr>
            @empty
                <tr>
                    <td colspan="4">Нет обращений</td>
                </tr>
            @endforelse
            </tbody>
        </table>
    </div>
    {{ $claims->links() }}
@endsection
