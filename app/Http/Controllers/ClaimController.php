<?php

namespace App\Http\Controllers;

use App\Models\Claim;
use App\Models\Reason;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ClaimController extends Controller
{

    public function index(Request $request)
    {
        if ($request->method() == 'GET') {
            $claims = Claim::query()->simplePaginate('10');
        } else {
            $claims = Claim::query()
                ->where('name', 'LIKE', '%'. $request->search . '%')
                ->orWhere('phone', 'LIKE', '%'. $request->search . '%')
                ->orWhere('hospital', 'LIKE', '%'. $request->search . '%')
                ->simplePaginate('10');
        }

        return view('claims.list')->with('claims', $claims);
    }


    public function create()
    {
        $reasons = Reason::query()->get();
        return view('claims.create')->with('reasons', $reasons);
    }

    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|max:255',
            'phone' => 'required|max:255',
            'hospital' => 'required|max:255',
            'reason_id' => 'integer',
            'content' => 'required',
        ];

        $massages = [
            'name.required' => 'Заполните поле ФИО',
            'name.max' => 'ФИО максимум 255 символов',
            'phone.required' => 'Заполните поле Телефон',
            'phone.max' => 'Телефон максимум 255 символов',
            'hospital.required' => 'Заполните поле Поликлиника',
            'hospital.max' => 'Поликлиника максимум 255 символов',
            'reason_id.integer' => 'Заполните поле Повод',
            'content.required' => 'Заполните поле Сообщение',
        ];

        Validator::make($request->all(), $rules, $massages)->validate();

        $claim = new Claim();
        $claim->name = $request->get('name');
        $claim->phone = $request->get('phone');
        $claim->hospital = $request->get('hospital');
        $claim->reason_id = $request->get('reason_id');
        $claim->content = $request->get('content');
        $claim->save();

        return redirect()->route('claim.list');
    }

    public function view(Request $request)
    {
        $claim = Claim::query()->find($request->id);
        return view('claims.view')->with('claim', $claim);
    }

}
